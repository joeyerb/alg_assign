#!/usr/bin/env python3

def maximum_subarray(arr):
    if len(arr) == 1:
        return arr
    mid = int (len(arr) / 2)
    arr_l = maximum_subarray(arr[:mid])
    arr_r = maximum_subarray(arr[mid:])
    arr_m = max_cross(arr)
    return max(arr_l, arr_m, arr_r, key=sum)

def max_cross(arr):
    mid = int (len(arr) / 2)
    left_sum = right_sum = 0
    left_sum_max = arr[mid-1]
    right_sum_max = arr[mid]
    lp = mid-1
    rp = mid
    for i in range(0, mid):
        left_sum += arr[mid-i-1]
        if left_sum > left_sum_max:
            lp = mid-i-1
            left_sum_max = left_sum
    for j in range(mid, len(arr)):
        right_sum += arr[j]
        if right_sum > right_sum_max:
            rp = j
            right_sum_max = right_sum
    return arr[lp:rp+1]

max_arr = maximum_subarray([1, -4, 1, 3, -1, 2])
print((sum(max_arr), max_arr))

def maximum_subarray_Kadane(arr):
    max_ending_here = max_so_far = 0
    start_ = 0
    for x in arr:
        if max_ending_here + x < 0:
            start_ = arr.index(x) + 1
            max_ending_here = 0
        else:
            max_ending_here += x

        if (max_ending_here > max_so_far):
            max_so_far = max_ending_here
            start = start_
            end = arr.index(x) + 1

    return (max_so_far, arr[start:end])

print(maximum_subarray_Kadane([1, -4, 1, 3, -1, 2]))
