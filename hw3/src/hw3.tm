<TeXmacs|1.99.2>

<style|generic>

<\body>
  <doc-data|<doc-title|Algorithm Analysis and Design>>

  <section*|Week 6-8: Divide-and-Conquer>

  <subsection|Counting Inversions>

  <\itemize>
    <item>Code

    <\cpp-code>
      #!/usr/bin/env python3

      \;

      def counting_inversions(arr, lo, hi):

      \ \ \ \ if hi == lo + 1:

      \ \ \ \ \ \ \ \ return 0

      \;

      \ \ \ \ mid = int ((lo + hi) / 2)

      \;

      \ \ \ \ n_left = counting_inversions(arr, lo, mid)

      \ \ \ \ n_right = counting_inversions(arr, mid, hi)

      \;

      \ \ \ \ n_mid = 0

      \ \ \ \ for i in reversed(range(lo, mid)):

      \ \ \ \ \ \ \ \ for j in range(i+1, hi):

      \ \ \ \ \ \ \ \ \ \ \ \ if arr[i] \<gtr\> arr[j]:

      \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ print('arr[', i, '\<gtr\>', j, ']')

      \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ n_mid += 1

      \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ if j+1 == hi:

      \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ arr.insert(1+j, arr[i])

      \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ arr.remove(arr[i])

      \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ print('\\tmove =\<gtr\>\\t',
      arr)

      \ \ \ \ \ \ \ \ \ \ \ \ else:

      \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ arr.insert(j, arr[i])

      \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ arr.remove(arr[i])

      \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ print('\\tmove =\<gtr\>\\t', arr)

      \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ break

      \;

      \ \ \ \ #print(lo, hi, n_left, n_mid, n_right)

      \ \ \ \ return n_left + n_mid + n_right

      \;

      print(counting_inversions([2, 4, 1, 3, 5, 0], 0, 6))

      \;
    </cpp-code>

    <item>Output

    <\cpp-code>
      arr[ 1 \<gtr\> 2 ]

      \ \ \ \ \ \ \ \ move =\<gtr\> \ [2, 1, 4, 3, 5, 0]

      arr[ 0 \<gtr\> 1 ]

      \ \ \ \ \ \ \ \ move =\<gtr\> \ [1, 2, 4, 3, 5, 0]

      arr[ 4 \<gtr\> 5 ]

      \ \ \ \ \ \ \ \ move =\<gtr\> \ [1, 2, 4, 3, 0, 5]

      arr[ 3 \<gtr\> 4 ]

      \ \ \ \ \ \ \ \ move =\<gtr\> \ [1, 2, 4, 0, 3, 5]

      arr[ 2 \<gtr\> 3 ]

      arr[ 2 \<gtr\> 4 ]

      \ \ \ \ \ \ \ \ move =\<gtr\> \ [1, 2, 0, 3, 4, 5]

      arr[ 1 \<gtr\> 2 ]

      \ \ \ \ \ \ \ \ move =\<gtr\> \ [1, 0, 2, 3, 4, 5]

      arr[ 0 \<gtr\> 1 ]

      \ \ \ \ \ \ \ \ move =\<gtr\> \ [0, 1, 2, 3, 4, 5]

      8

      \;
    </cpp-code>

    \;
  </itemize>

  <subsection|Maximum Subarray>

  D&C and Kadane's algorithm

  <\itemize>
    <item>Code

    <\cpp-code>
      #!/usr/bin/env python3

      \;

      ## Divide-and-Conquer

      def maximum_subarray(arr):

      \ \ \ \ if len(arr) == 1:

      \ \ \ \ \ \ \ \ return arr

      \ \ \ \ mid = int (len(arr) / 2)

      \ \ \ \ arr_l = maximum_subarray(arr[:mid])

      \ \ \ \ arr_r = maximum_subarray(arr[mid:])

      \ \ \ \ arr_m = max_cross(arr)

      \ \ \ \ return max(arr_l, arr_m, arr_r, key=sum)

      \;

      def max_cross(arr):

      \ \ \ \ mid = int (len(arr) / 2)

      \ \ \ \ left_sum = right_sum = 0

      \ \ \ \ left_sum_max = arr[mid-1]

      \ \ \ \ right_sum_max = arr[mid]

      \ \ \ \ lp = mid-1

      \ \ \ \ rp = mid

      \ \ \ \ for i in range(0, mid):

      \ \ \ \ \ \ \ \ left_sum += arr[mid-i-1]

      \ \ \ \ \ \ \ \ if left_sum \<gtr\> left_sum_max:

      \ \ \ \ \ \ \ \ \ \ \ \ lp = mid-i-1

      \ \ \ \ \ \ \ \ \ \ \ \ left_sum_max = left_sum

      \ \ \ \ for j in range(mid, len(arr)):

      \ \ \ \ \ \ \ \ right_sum += arr[j]

      \ \ \ \ \ \ \ \ if right_sum \<gtr\> right_sum_max:

      \ \ \ \ \ \ \ \ \ \ \ \ rp = j

      \ \ \ \ \ \ \ \ \ \ \ \ right_sum_max = right_sum

      \ \ \ \ return arr[lp:rp+1]

      \;

      max_arr = maximum_subarray([1, -4, 1, 3, -1, 2])

      print((sum(max_arr), max_arr))

      \;

      \;

      ## Kadane's Algorithm

      def maximum_subarray_Kadane(arr):

      \ \ \ \ max_ending_here = max_so_far = 0

      \ \ \ \ start_ = 0

      \ \ \ \ for x in arr:

      \ \ \ \ \ \ \ \ if max_ending_here + x \<less\> 0:

      \ \ \ \ \ \ \ \ \ \ \ \ start_ = arr.index(x) + 1

      \ \ \ \ \ \ \ \ \ \ \ \ max_ending_here = 0

      \ \ \ \ \ \ \ \ else:

      \ \ \ \ \ \ \ \ \ \ \ \ max_ending_here += x

      \;

      \ \ \ \ \ \ \ \ if (max_ending_here \<gtr\> max_so_far):

      \ \ \ \ \ \ \ \ \ \ \ \ max_so_far = max_ending_here

      \ \ \ \ \ \ \ \ \ \ \ \ start = start_

      \ \ \ \ \ \ \ \ \ \ \ \ end = arr.index(x) + 1

      \;

      \ \ \ \ return (max_so_far, arr[start:end])

      \;

      print(maximum_subarray_Kadane([1, -4, 1, 3, -1, 2]))

      \;
    </cpp-code>

    <item>Output

    <\cpp-code>
      (5, [1, 3, -1, 2])

      (5, [1, 3, -1, 2])

      \;
    </cpp-code>

    \;
  </itemize>

  <subsection|Closest Pair 2D>

  <\itemize>
    <item>Code:
  </itemize>

  <\cpp-code>
    #!/usr/bin/env python3

    \;

    # Brute-force

    def closest_pair_2d_bf (points):

    \ \ \ \ d = dist_2d (points[0], points[1])

    \ \ \ \ for point_1 in points:

    \ \ \ \ \ \ \ \ for point_2 in points:

    \ \ \ \ \ \ \ \ \ \ \ \ if point_1 != point_2:

    \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ d_ = dist_2d (point_1, point_2)

    \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ if d_ \<less\> d:

    \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ d = d_

    \ \ \ \ return d

    \;

    # Divide-and-Conquer

    def closest_pair_2d (points):

    \ \ \ \ num = len (points)

    \ \ \ \ mid = int (num / 2)

    \;

    \ \ \ \ if num == 2:

    \ \ \ \ \ \ \ \ return dist_2d (points[0], points[1])

    \;

    \ \ \ \ if num == 3:

    \ \ \ \ \ \ \ \ d1 = dist_2d (points[0], points[1])

    \ \ \ \ \ \ \ \ d2 = dist_2d (points[1], points[2])

    \ \ \ \ \ \ \ \ d3 = dist_2d (points[2], points[0])

    \ \ \ \ \ \ \ \ return min (d1, d2, d3)

    \;

    \ \ \ \ # Left and Right

    \ \ \ \ d1 = closest_pair_2d (points[:mid])

    \ \ \ \ d2 = closest_pair_2d (points[mid:])

    \ \ \ \ d = min (d1, d2)

    \;

    \ \ \ \ # Middle

    \ \ \ \ mid_points = []

    \ \ \ \ i = mid

    \ \ \ \ if i \<gtr\>= 0 and points[mid][0] - points[i][0] \<less\>= d:
    \ \ # d[0]

    \ \ \ \ \ \ \ \ mid_points.append(points[i])

    \ \ \ \ \ \ \ \ i -= 1

    \ \ \ \ i = mid

    \ \ \ \ if points[i][0] - points[mid][0] \<less\> d: \ \ # d[0]

    \ \ \ \ \ \ \ \ mid_points.append(points[i])

    \ \ \ \ \ \ \ \ i += 1

    \ \ \ \ mid_points = sorted (mid_points, key=lambda p: p[1])

    \ \ \ \ n = len(mid_points)

    \ \ \ \ for i in range(0, n):

    \ \ \ \ \ \ \ \ j = i + 1

    \ \ \ \ \ \ \ \ while j \<less\> i + 8 \<less\> n:

    \ \ \ \ \ \ \ \ \ \ \ \ d_ = dist_2d (mid_points[i], mid_points[j])

    \ \ \ \ \ \ \ \ \ \ \ \ if d_ \<less\> d:

    \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ d = d_

    \ \ \ \ \ \ \ \ \ \ \ \ j += 1

    \;

    \ \ \ \ return d

    \;

    def dist_2d (p1, p2):

    \ \ \ \ return (p1[0] - p2[0]) ** 2 + (p1[1] - p2[1]) ** 2

    \;

    # Or return a tuple (dist, p1, p2)

    def dist_2d_ (p1, p2):

    \ \ \ \ return ((p1[0] - p2[0]) ** 2 + (p1[1] - p2[1]) ** 2, p1, p2)

    \;

    points = [(1, 2), (17, 11), (11, 0), (26, 33), (38, 9)]

    print (closest_pair_2d (sorted (points)))

    print (closest_pair_2d_bf (points))

    \;
  </cpp-code>

  <\itemize>
    <item>Output:
  </itemize>

  <\scm-code>
    104

    104

    \;
  </scm-code>

  \;
</body>

<\references>
  <\collection>
    <associate|CITEREFGilbergForouzan2001|<tuple|<with|mode|<quote|math>|\<bullet\>>|?>>
    <associate|auto-1|<tuple|?|1>>
    <associate|auto-10|<tuple|2|7>>
    <associate|auto-11|<tuple|2|7>>
    <associate|auto-12|<tuple|2|?>>
    <associate|auto-13|<tuple|2|?>>
    <associate|auto-14|<tuple|1.5|?>>
    <associate|auto-15|<tuple|1.6|?>>
    <associate|auto-16|<tuple|2|?>>
    <associate|auto-2|<tuple|1|1>>
    <associate|auto-3|<tuple|2|3>>
    <associate|auto-4|<tuple|3|3>>
    <associate|auto-5|<tuple|4|4>>
    <associate|auto-6|<tuple|2|4>>
    <associate|auto-7|<tuple|2|5>>
    <associate|auto-8|<tuple|2|6>>
    <associate|auto-9|<tuple|2|6>>
    <associate|footnote-1|<tuple|1|?>>
    <associate|footnr-1|<tuple|1|?>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|toc>
      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|Week
      6: Divide-and-Conquer I> <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-1><vspace|0.5fn>

      <with|par-left|<quote|1tab>|1<space|2spc>Counting Inversions
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-2>>

      <with|par-left|<quote|1tab>|2<space|2spc>Permutation
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-3>>

      <with|par-left|<quote|1tab>|3<space|2spc>BST Search
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-4>>

      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|Week
      5> <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-5><vspace|0.5fn>

      <with|par-left|<quote|1tab>|1<space|2spc>Master Method
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-6>>

      <with|par-left|<quote|1tab>|2<space|2spc>Integer Partition
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-7>>
    </associate>
  </collection>
</auxiliary>