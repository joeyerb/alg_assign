#!/usr/bin/env python3

def counting_inversions(arr, lo, hi):
    if hi == lo + 1:
        return 0

    mid = int ((lo + hi) / 2)

    n_left = counting_inversions(arr, lo, mid)
    n_right = counting_inversions(arr, mid, hi)

    n_mid = 0
    for i in reversed(range(lo, mid)):
        for j in range(i+1, hi):
            if arr[i] > arr[j]:
                print('arr[', i, '>', j, ']')
                n_mid += 1
                if j+1 == hi:
                    arr.insert(1+j, arr[i])
                    arr.remove(arr[i])
                    print('\tmove =>\t', arr)
            else:
                arr.insert(j, arr[i])
                arr.remove(arr[i])
                print('\tmove =>\t', arr)
                break

    #print(lo, hi, n_left, n_mid, n_right)
    return n_left + n_mid + n_right

print(counting_inversions([2, 4, 1, 3, 5, 0], 0, 6))
