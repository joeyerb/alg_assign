#!/usr/bin/env python3

import heapq
import random

graph = []
N = 27  # graph size

# Create a random graph
for i in range(N):
    line = []
    for j in range(i):
        if random.randint(1, 10) > 5:   # 50% => 0
            w = random.randint(1, 100)
            line.append(w)
        else:
            line.append(0)
    line.append(0)
    graph.append(line)
for i in range(N):
    for j in range(i+1, N):
        graph[i].append(graph[j][i])

# print graph
for i in range(N):
    print(i, graph[i])
print()

n = len(graph)

# start from vertex 0
v0 = 0
v_found = [v0]
e_found = []
pq = []
w_sum = 0   # weight sum

# function to add v's edges to pq
def add_to_pq(v):
    for u in range(n):
        if u not in v_found and graph[v][u] > 0:
            heapq.heappush(pq, (graph[v][u], v, u))

# first add v0's
add_to_pq(v0)

# loop until pq is empty
while pq:
    w = pq[0][0]
    v = pq[0][1]
    u = pq[0][2]
    #print('pq', pq)
    heapq.heappop(pq)
    if u not in v_found:
        v_found.append(u)
        e_found.append((v, u))
        add_to_pq(u)
        w_sum += w

print(w_sum, e_found)
