#!/usr/bin/env python3

import random

def bucket_sort(array):
    n = len(array)
    buckets = [0] * 10000
    for i in range(n):
        buckets[array[i]] += 1 
    result = []
    for i in range(10000):
        if buckets[i] > 0:
            for j in range(buckets[i]):
                result.append(i)
    print(result)

array = []
for i in range(100):
    array.append(random.randint(1, 10000))

bucket_sort(array)
