#!/usr/bin/env python3

def matrix_parenthesization(p):
    n = len(p) - 1
    m = {}
    s = {}
    for i in range(1, n+1):
        m[(i, i)] = 0

    for l in range(2, n+1):
        for i in range(1, n-l+2):
            j = i + l - 1
            m[(i, j)] = float('inf')
            for k in range(i, j):
                q = m[(i, k)] + m[(k+1, j)] + p[i-1] * p[k] * p[j]
                if q < m[(i, j)]:
                    m[(i, j)] = q
                    s[(i, j)] = k

    print(m[(1, n)], ':', multiply(s, 1, n))


def multiply(s, i, j):
    if i == j:
        return 'A' + str(i)
    else:
        k = s[(i, j)]
        x = multiply(s, i, k)
        y = multiply(s, k+1, j)
        return '(' + x + ' ' + y + ')'


# Test
p = [30, 35, 15, 5, 10, 20, 25]     # A1: 30x35, A2: 35x15, ...
matrix_parenthesization(p)      # =>  15125 : ((A1 (A2 A3)) ((A4 A5) A6)


## 20 x [100, 500]

import random

p = []
for _ in range(21):
    p.append(random.randint(100, 500))
print(p)

matrix_parenthesization(p)
