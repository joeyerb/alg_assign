#!/usr/bin/env python3

class Solution:

    # @param {string} s
    # @return {string}
    def shortestPalindrome(self, s, k=1):
        n = len(s)
        j = 0
        a = k + 1
        i = k
        while i <= n and i + j < n and s[-i] == s[j]:
            i += 1
            j += 1
            if a == k + 1 and s[-i] == s[0]:
                a = i
        if i + j == n or i + j == n + 1:
            t = ''
            for i in range(1, k):
                t += s[-i]
            return t + s
        else:
            return self.shortestPalindrome(s, a)

    def shortestPalindromeK(self, s):
        A=s+s[::-1]
        cont=[0]
        for i in range(1,len(A)):
            index=cont[i-1]
            while(index>0 and A[index]!=A[i]):
                index=cont[index-1]
            cont.append(index+(1 if A[index]==A[i] else 0))
        print(cont)
        return s[cont[-1]:][::-1]+s



s = Solution()
print(s.shortestPalindromeK('aaabcaaa'))


