#!/usr/bin/env python3

class Solution:

    # 171. Excel Sheet Column Number

    ## Given a column title as appear in an Excel sheet,
    ## return its corresponding column number.

    # @param {string} s
    # @return {integer}

    def titleToNumber(self, s):
        n = 0
        for c in s.upper():
            n = n * 26 + ord(c) - 64
        return n


    # 172. Factorial Trailing Zeroes

    ## Given an integer n, return the number of trailing zeroes in n!.

    # @param {integer} n
    # @return {integer}

    def trailingZeroes(self, n):
        return n // 5 + self.trailingZeroes(n // 5) if n else 0


# Tests
s = Solution()

print(s.titleToNumber('AAA'))

for i in range(24, 26):
    print(i, s.trailingZeroes(i))
