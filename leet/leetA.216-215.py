#!/usr/bin/env python3

class Solution:

    # 216. Combination Sum III

    ## Find all possible combinations of k numbers that add up to a number n,
    ## given that only numbers from 1 to 9 can be used and each combination
    ## should be a unique set of numbers.

    # @param {integer} k
    # @param {integer} n
    # @return {integer[][]}
    def combinationSum3(self, k, n, i=1):
        '''
        f(3, 9) => [[1, 2, 6], [1, 3, 5], [2, 3, 4]]
            f(2, 8) => [[2, 6], [3, 5]]
                f(1, 6) => [[6]]
                f(1, 5) => [[5]]
            f(2, 7) => [[3, 4]]
                f(1, 4) => [[4]]
        '''
        r = []
        if k == 1 and i <= n < 10:
            return [[n]]
        if k > 1 and i < 10:
            for j in range(i, 10):
                for t in self.combinationSum3(k-1, n-j, j+1):
                    r.append([j] + t)
        return r


    # 215. Kth Largest Element in an Array

    ## Find the kth largest element in an unsorted array.
    ## Note that it is the kth largest element in the sorted order,
    ## not the kth distinct element. 

    # @param {integer[]} nums
    # @param {integer} k
    # @return {integer}
    def findKthLargest(self, nums, k):
        m = nums[0]
        b = 1
        a = []
        c = []
        for n in nums[1:]:
            if n < m:
                a.append(n)
            elif n == m:
                b += 1
            else:
                c.append(n)
        nc = len(c)
        if nc >= k:
            return self.findKthLargest(c, k)
        elif nc < k <= nc + b:
            return m
        else:
            return self.findKthLargest(a, k-b-nc)


# Tests
s = Solution()
print(s.combinationSum3(3, 9))  # => [[1, 2, 6], [1, 3, 5], [2, 3, 4]]
print(s.findKthLargest([3,2,1,5,6,4], 2))   # => 5

