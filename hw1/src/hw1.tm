<TeXmacs|1.99.2>

<style|generic>

<\body>
  <doc-data|<doc-title|Algorithm Analysis and Design>>

  <section*|Week 1>

  <subsection|Peak Finder 1D and 2D>

  <\itemize>
    <item>Code:
  </itemize>

  <\cpp-code>
    #!/usr/bin/env python2

    \;

    def find_peak(arr, first, last):

    \ \ \ \ mid = (first + last) / 2

    \ \ \ \ if arr[mid-1] \<gtr\> arr[mid]:

    \ \ \ \ \ \ \ \ return find_peak(arr, first, mid-1)

    \ \ \ \ elif arr[mid] \<less\> arr[mid+1]:

    \ \ \ \ \ \ \ \ return find_peak(arr, mid+1, last)

    \ \ \ \ else:

    \ \ \ \ \ \ \ \ return (mid, arr[mid])

    \;

    def find_max_in_col(mtrx, col):

    \ \ \ \ _max = -1

    \ \ \ \ m_row = 0

    \ \ \ \ for i in range(1, len(mtrx)-1):

    \ \ \ \ \ \ \ \ if mtrx[i][col] \<gtr\> _max:

    \ \ \ \ \ \ \ \ \ \ \ \ _max = mtrx[i][col]

    \ \ \ \ \ \ \ \ \ \ \ \ m_row = i

    \ \ \ \ return (m_row, _max)

    \;

    def find_peak2(mtrx, s_row, e_row, s_col, e_col):

    \ \ \ \ mid_row = (s_row + e_row) / 2

    \ \ \ \ mid_col = (s_col + e_col) / 2

    \ \ \ \ row_max = max(mtrx[mid_row])

    \ \ \ \ m_col = mtrx[mid_row].index(row_max)

    \ \ \ \ (m_row, col_max) = find_max_in_col(mtrx, mid_col)

    \ \ \ \ if row_max \<gtr\>= col_max:

    \ \ \ \ \ \ \ \ if mtrx[m_col][mid_row-1] \<gtr\> mtrx[m_col][mid_row]:

    \ \ \ \ \ \ \ \ \ \ \ \ if m_col \<less\> mid_col: \ \ \ \ # top left

    \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ return find_peak2(mtrx, s_row, mid_row,
    s_col, mid_col)

    \ \ \ \ \ \ \ \ \ \ \ \ else: \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ # top
    right

    \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ return find_peak2(mtrx, s_row, mid_row,
    mid_col, e_col)

    \ \ \ \ \ \ \ \ elif mtrx[m_col][mid_row+1] \<gtr\> mtrx[m_col][mid_row]:

    \ \ \ \ \ \ \ \ \ \ \ \ if m_col \<less\> mid_col: \ \ \ \ # bottom left

    \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ return find_peak2(mtrx, mid_row, e_row,
    s_col, mid_col)

    \ \ \ \ \ \ \ \ \ \ \ \ else: \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ #
    bottom right

    \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ return find_peak2(mtrx, mid_row, s_row,
    mid_col, e_col)

    \ \ \ \ \ \ \ \ else:

    \ \ \ \ \ \ \ \ \ \ \ \ return ((m_col, mid_row), mtrx[m_col][mid_row])

    \ \ \ \ else:

    \ \ \ \ \ \ \ \ if mtrx[mid_col-1][m_row] \<gtr\> mtrx[mid_col][m_row]:

    \ \ \ \ \ \ \ \ \ \ \ \ if m_row \<less\> mid_row: \ \ \ \ # top left

    \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ return find_peak2(mtrx, s_row, mid_row,
    s_col, mid_col)

    \ \ \ \ \ \ \ \ \ \ \ \ else: \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ #
    bottom left

    \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ return find_peak2(mtrx, s_row, mid_row,
    mid_col, e_col)

    \ \ \ \ \ \ \ \ elif mtrx[mid_col+1][m_row] \<gtr\> mtrx[mid_col][m_row]:

    \ \ \ \ \ \ \ \ \ \ \ \ if m_row \<less\> mid_row: \ \ \ \ # top right

    \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ return find_peak2(mtrx, mid_row, e_row,
    s_col, mid_col)

    \ \ \ \ \ \ \ \ \ \ \ \ else: \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ #
    bottom right

    \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ return find_peak2(mtrx, mid_row, s_row,
    mid_col, e_col)

    \ \ \ \ \ \ \ \ else:

    \ \ \ \ \ \ \ \ \ \ \ \ return ((mid_col, m_row), mtrx[mid_col][m_row])

    \ \ \ \ 

    \;

    def main():

    \ \ \ \ arr = [10, 3, 3, 2, 8, 7, 5, 6, 5, 4, 3]

    \ \ \ \ new_arr = [-1] + arr + [-1]

    \ \ \ \ print(new_arr)

    \ \ \ \ print(find_peak(new_arr, 1, len(arr)))

    \;

    \ \ \ \ print

    \ \ \ \ mtrx = [

    \ \ \ \ \ \ \ \ [ 4, \ 5, \ 6, \ 7, \ 8, \ 7, \ 6, \ 5, \ 4, \ 3, \ 2],

    \ \ \ \ \ \ \ \ [ 5, \ 6, \ 7, \ 8, \ 9, \ 8, \ 7, \ 6, \ 5, \ 4, \ 3],

    \ \ \ \ \ \ \ \ [ 6, \ 7, \ 8, \ 9, 10, \ 9, \ 8, \ 7, \ 6, \ 5, \ 4],

    \ \ \ \ \ \ \ \ [ 7, \ 8, \ 9, 10, 11, 10, \ 9, \ 8, \ 7, \ 6, \ 5],

    \ \ \ \ \ \ \ \ [ 8, \ 9, 10, 11, 12, 11, 10, \ 9, \ 8, \ 7, \ 6],

    \ \ \ \ \ \ \ \ [ 7, \ 8, \ 9, 10, 11, 10, \ 9, \ 8, \ 7, \ 6, \ 5],

    \ \ \ \ \ \ \ \ [ 6, \ 7, \ 8, \ 9, 10, \ 9, \ 8, \ 7, \ 6, \ 5, \ 4],

    \ \ \ \ \ \ \ \ [ 5, \ 6, \ 7, \ 8, \ 9, \ 8, \ 7, \ 6, \ 5, \ 4, \ 3],

    \ \ \ \ \ \ \ \ [ 4, \ 5, \ 6, \ 7, \ 8, \ 7, \ 6, \ 5, \ 4, \ 3, \ 2],

    \ \ \ \ \ \ \ \ [ 3, \ 4, \ 5, \ 6, \ 7, \ 6, \ 5, \ 4, \ 3, \ 2, \ 1],

    \ \ \ \ \ \ \ \ [ 2, \ 3, \ 4, \ 5, \ 6, \ 5, \ 4, \ 3, \ 2, \ 1, \ 0]

    \ \ \ \ ]

    \ \ \ \ mtrx = [

    \ \ \ \ \ \ \ \ [ 0, \ 0, \ 0, \ 1, \ 0, \ 0, 0],

    \ \ \ \ \ \ \ \ [ 0, \ 4, \ 3, \ 2, \ 0, \ 1, 0],

    \ \ \ \ \ \ \ \ [ 0, \ 5, \ 0, \ 1, \ 1, \ 2, 0],

    \ \ \ \ \ \ \ \ [ 0, \ 6, \ 1, \ 0, \ 0, \ 3, 0],

    \ \ \ \ \ \ \ \ [ 0, \ 7, \ 0, \ 0, \ 0, 13, 0],

    \ \ \ \ \ \ \ \ [ 0, \ 8, \ 9, 10, 11, 12, 0],

    \ \ \ \ \ \ \ \ [ 0, \ 0, \ 0, \ 0, \ 0, \ 0, 0]

    \ \ \ \ ]

    \ \ \ \ row0 = []

    \ \ \ \ n_row = len(mtrx)

    \ \ \ \ n_col = len(mtrx[0])

    \ \ \ \ for col in range(0, n_col):

    \ \ \ \ \ \ \ \ row0 += [-1]

    \ \ \ \ new_mtrx = [row0]

    \ \ \ \ for row in mtrx:

    \ \ \ \ \ \ \ \ new_mtrx += [[-1] + row + [-1]]

    \ \ \ \ new_mtrx += [row0]

    \ \ \ \ for row in new_mtrx:

    \ \ \ \ \ \ \ \ print(row)

    \ \ \ \ print(find_peak2(new_mtrx, 1, n_row, 1, n_col))

    \;

    if __name__ == '__main__':

    \ \ \ \ main()

    \;
  </cpp-code>

  <new-page*>

  <\itemize>
    <item>Output:
  </itemize>

  <\cpp-code>
    [-1, 10, 3, 3, 2, 8, 7, 5, 6, 5, 4, 3, -1]

    (3, 3)

    \;

    [-1, -1, -1, -1, -1, -1, -1]

    [-1, 0, 0, 0, 1, 0, 0, 0, -1]

    [-1, 0, 4, 3, 2, 0, 1, 0, -1]

    [-1, 0, 5, 0, 1, 1, 2, 0, -1]

    [-1, 0, 6, 1, 0, 0, 3, 0, -1]

    [-1, 0, 7, 0, 0, 0, 13, 0, -1]

    [-1, 0, 8, 9, 10, 11, 12, 0, -1]

    [-1, 0, 0, 0, 0, 0, 0, 0, -1]

    [-1, -1, -1, -1, -1, -1, -1]

    ((5, 6), 13)

    \;
  </cpp-code>

  <section*|Week 2>

  <\proposition>
    if <math|<below|lim|n\<rightarrow\>\<infty\>><dfrac|f<around*|(|n|)>|g<around*|(|n|)>>=C\<gtr\>0>,
    then <math|f<around*|(|n|)>=\<Theta\><around*|(|g<around*|(|n|)>|)>>
  </proposition>

  <\proof>
    <\math>
      f<around*|(|n|)>=C\<cdot\>g<around*|(|n|)>=O<around*|(|g<around*|(|n|)>|)>\<nocomma\>\<nocomma\>

      g<around*|(|n|)>=<dfrac|f<around*|(|n|)>|C>=O<around*|(|f<around*|(|n|)>|)>
      \<Rightarrow\> f<around*|(|n|)>=\<Omega\><around*|(|g<around*|(|n|)>|)>

      \<Rightarrow\>f<around*|(|n|)>=\<Theta\><around*|(|g<around*|(|n|)>|)>
    </math>
  </proof>

  <subsection|Sorted Merge>

  <\itemize>
    <item>Code:
  </itemize>

  <\cpp-code>
    #!/usr/bin/env python3

    \;

    def merge_ab(a, b):

    \ \ \ \ c = []

    \ \ \ \ ia = 0

    \ \ \ \ ib = 0

    \ \ \ \ while ia \<less\> len(a) and ib \<less\> len(b):

    \ \ \ \ \ \ \ \ if a[ia] \<less\> b[ib]:

    \ \ \ \ \ \ \ \ \ \ \ \ c.append(a[ia])

    \ \ \ \ \ \ \ \ \ \ \ \ ia += 1

    \ \ \ \ \ \ \ \ else:

    \ \ \ \ \ \ \ \ \ \ \ \ c.append(b[ib])

    \ \ \ \ \ \ \ \ \ \ \ \ ib += 1

    \ \ \ \ while ia \<less\> len(a):

    \ \ \ \ \ \ \ \ c.append(a[ia])

    \ \ \ \ \ \ \ \ ia += 1

    \ \ \ \ while ib \<less\> len(b):

    \ \ \ \ \ \ \ \ c.append(b[ib])

    \ \ \ \ \ \ \ \ ib += 1

    \ \ \ \ return c

    \;

    def main():

    \ \ \ \ a = [1, 3, 5, 7, 8, 9]

    \ \ \ \ b = [2, 3, 4, 6, 10, 11]

    \ \ \ \ print(merge_ab(a, b))

    \;

    if __name__ == '__main__':

    \ \ \ \ main()
  </cpp-code>

  <\itemize>
    <item>Output:
  </itemize>

  <\cpp-code>
    [1, 2, 3, 3, 4, 5, 6, 7, 8, 9, 10, 11]
  </cpp-code>

  <subsection|Closest Pair>

  <subsubsection|Closest Pair 1D>

  <\itemize>
    <item>Code:
  </itemize>

  <\cpp-code>
    #!/usr/bin/env python3

    \;

    def closest_pair_1d (points):

    \ \ \ \ num = len (points)

    \ \ \ \ mid = int (num / 2)

    \;

    \ \ \ \ if num == 2:

    \ \ \ \ \ \ \ \ return dist_1d (points[0], points[1])

    \;

    \ \ \ \ if num == 3:

    \ \ \ \ \ \ \ \ d1 = dist_1d (points[0], points[1])

    \ \ \ \ \ \ \ \ d2 = dist_1d (points[1], points[2])

    \ \ \ \ \ \ \ \ return min (d1, d2)

    \;

    \ \ \ \ d1 = closest_pair_1d (points[:mid])

    \ \ \ \ d2 = closest_pair_1d (points[mid:])

    \ \ \ \ d3 = dist_1d (points[mid-1], points[mid])

    \;

    \ \ \ \ return min (d1, d2, d3)

    \;

    def dist_1d (p1, p2):

    \ \ \ \ if p1 \<gtr\> p2:

    \ \ \ \ \ \ \ \ return p1 - p2

    \ \ \ \ else:

    \ \ \ \ \ \ \ \ return p2 - p1

    \;

    def main():

    \ \ \ \ points = [152, 111, 8, 23, 84, 34, 66, 132]

    \ \ \ \ print (closest_pair_1d (sorted (points)))

    \;

    if __name__ == '__main__':

    \ \ \ \ main()
  </cpp-code>

  <\itemize>
    <item>Output:
  </itemize>

  <\cpp-code>
    11
  </cpp-code>

  <subsubsection|Closest Pair 2D>

  <\itemize>
    <item>Code:
  </itemize>

  <\cpp-code>
    #!/usr/bin/env python3

    \;

    # Brute-force

    def closest_pair_2d_bf (points):

    \ \ \ \ d = dist_2d (points[0], points[1])

    \ \ \ \ for point_1 in points:

    \ \ \ \ \ \ \ \ for point_2 in points:

    \ \ \ \ \ \ \ \ \ \ \ \ if point_1 != point_2:

    \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ d_ = dist_2d (point_1, point_2)

    \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ if d_ \<less\> d:

    \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ d = d_

    \ \ \ \ return d

    \;

    # Divide-and-conquer

    def closest_pair_2d (points):

    \ \ \ \ num = len (points)

    \ \ \ \ mid = int (num / 2)

    \;

    \ \ \ \ if num == 2:

    \ \ \ \ \ \ \ \ return dist_2d (points[0], points[1])

    \;

    \ \ \ \ if num == 3:

    \ \ \ \ \ \ \ \ d1 = dist_2d (points[0], points[1])

    \ \ \ \ \ \ \ \ d2 = dist_2d (points[1], points[2])

    \ \ \ \ \ \ \ \ d3 = dist_2d (points[2], points[0])

    \ \ \ \ \ \ \ \ return min (d1, d2, d3)

    \;

    \ \ \ \ # Left and Right

    \ \ \ \ d1 = closest_pair_2d (points[:mid])

    \ \ \ \ d2 = closest_pair_2d (points[mid:])

    \ \ \ \ d = min (d1, d2)

    \;

    \ \ \ \ # Middle

    \ \ \ \ mid_points = []

    \ \ \ \ i = mid

    \ \ \ \ if i \<gtr\>= 0 and points[mid][0] - points[i][0] \<less\>= d:

    \ \ \ \ \ \ \ \ mid_points.append(points[i])

    \ \ \ \ \ \ \ \ i -= 1

    \ \ \ \ i = mid

    \ \ \ \ if points[i][0] - points[mid][0] \<less\> d:

    \ \ \ \ \ \ \ \ mid_points.append(points[i])

    \ \ \ \ \ \ \ \ i += 1

    \ \ \ \ mid_points = sorted (mid_points, key=lambda p: p[1])

    \ \ \ \ n = len(mid_points)

    \ \ \ \ for i in range(0, n):

    \ \ \ \ \ \ \ \ j = i - 6

    \ \ \ \ \ \ \ \ while j \<less\> 0:

    \ \ \ \ \ \ \ \ \ \ \ \ j += 1

    \ \ \ \ \ \ \ \ while j \<less\> i + 7 and i + 7 \<less\> n:

    \ \ \ \ \ \ \ \ \ \ \ \ d_ = dist_2d (mid_points[i], mid_points[j])

    \ \ \ \ \ \ \ \ \ \ \ \ if d_ \<less\> d:

    \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ d = d_

    \ \ \ \ \ \ \ \ \ \ \ \ j += 1

    \;

    \ \ \ \ return d

    \;

    def dist_2d (p1, p2):

    \ \ \ \ return (p1[0] - p2[0]) ** 2 + (p1[1] - p2[1]) ** 2

    \;

    # Or return a tuple (dist, p1, p2)

    def dist_2d_ (p1, p2):

    \ \ \ \ return ((p1[0] - p2[0]) ** 2 + (p1[1] - p2[1]) ** 2, p1, p2)

    \;

    def main():

    \ \ \ \ points = [(1, 2), (17, 11), (11, 0), (26, 33), (38, 9)]

    \ \ \ \ print (closest_pair_2d (sorted (points)))

    \ \ \ \ print (closest_pair_2d_bf (points))

    \;

    if __name__ == '__main__':

    \ \ \ \ main()

    \;
  </cpp-code>

  <\itemize>
    <item>Output:
  </itemize>

  <\cpp-code>
    104

    104
  </cpp-code>

  <section*|Week 3>

  <subsection|Fibonacci>

  <\itemize>
    <item>Code:
  </itemize>

  <\scm-code>
    #lang racket

    \;

    (define (fib1 n)

    \ \ (if (\<less\> n 3)

    \ \ \ \ \ \ 1

    \ \ \ \ \ \ (+ (fib1 (- n 1)) (fib1 (- n 2)))))

    \;

    \;

    (define (fib2 n)

    \ \ (letrec ([fib_helper (lambda (a b n)

    \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ (if (\<gtr\> n 0)

    \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ (fib_helper b
    (+ a b) (- n 1))

    \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ a))])

    \ \ \ \ (fib_helper 0 1 n)))

    \;

    (fib1 38)

    (fib2 4444)

    \;
  </scm-code>

  <\itemize>
    <item>Output:
  </itemize>

  <\scm-code>
    39088169

    24637025257231777654607449416959223171154340237404734920997583113...
  </scm-code>

  <subsection|Tower of Hanoi>

  <\itemize>
    <item>Code:
  </itemize>

  <\scm-code>
    #lang racket

    \;

    (define (hanoi n src dst via)

    \ \ (if (zero? n)

    \ \ \ \ \ \ empty

    \ \ \ \ \ \ (append (hanoi (sub1 n) src via dst)

    \ \ \ \ \ \ \ \ \ \ \ \ \ \ (cons (list 'move n src '-\<gtr\> dst)

    \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ (hanoi (sub1 n) via dst src)))))

    \;

    (hanoi 4 'A 'C 'B)

    \;

    ;; Or

    (define (h n)

    \ \ (hanoi n 'A 'C 'B))

    \;

    (h 4)

    \;
  </scm-code>

  <\itemize>
    <item>Output:
  </itemize>

  <\scm-code>
    '((move 1 A -\<gtr\> B)

    \ \ (move 2 A -\<gtr\> C)

    \ \ (move 1 B -\<gtr\> C)

    \ \ (move 3 A -\<gtr\> B)

    \ \ (move 1 C -\<gtr\> A)

    \ \ (move 2 C -\<gtr\> B)

    \ \ (move 1 A -\<gtr\> B)

    \ \ (move 4 A -\<gtr\> C)

    \ \ (move 1 B -\<gtr\> C)

    \ \ (move 2 B -\<gtr\> A)

    \ \ (move 1 C -\<gtr\> A)

    \ \ (move 3 B -\<gtr\> C)

    \ \ (move 1 A -\<gtr\> B)

    \ \ (move 2 A -\<gtr\> C)

    \ \ (move 1 B -\<gtr\> C))
  </scm-code>

  \;
</body>

<\references>
  <\collection>
    <associate|auto-1|<tuple|?|1>>
    <associate|auto-10|<tuple|2|7>>
    <associate|auto-11|<tuple|3.2|7|../../../.TeXmacs/texts/scratch/no_name_6.tm>>
    <associate|auto-2|<tuple|1|1>>
    <associate|auto-3|<tuple|<with|mode|<quote|math>|\<bullet\>>|3>>
    <associate|auto-4|<tuple|1|3>>
    <associate|auto-5|<tuple|2|4>>
    <associate|auto-6|<tuple|2.1|4>>
    <associate|auto-7|<tuple|2.2|5>>
    <associate|auto-8|<tuple|<with|mode|<quote|math>|\<bullet\>>|6>>
    <associate|auto-9|<tuple|1|6>>
    <associate|footnote-1|<tuple|1|?|../../../.TeXmacs/texts/scratch/no_name_6.tm>>
    <associate|footnr-1|<tuple|1|?|../../../.TeXmacs/texts/scratch/no_name_6.tm>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|toc>
      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|Week
      1> <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-1><vspace|0.5fn>

      <with|par-left|<quote|1tab>|1<space|2spc>Peak Finder 1D and 2D
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-2>>

      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|Week
      2> <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-3><vspace|0.5fn>

      <with|par-left|<quote|1tab>|1<space|2spc>Sorted Merge
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-4>>

      <with|par-left|<quote|1tab>|2<space|2spc>Closest Pair
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-5>>

      <with|par-left|<quote|2tab>|2.1<space|2spc>Closest Pair 1D
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-6>>

      <with|par-left|<quote|2tab>|2.2<space|2spc>Closest Pair 2D
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-7>>

      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|Week
      3> <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-8><vspace|0.5fn>

      <with|par-left|<quote|1tab>|1<space|2spc>Fibonacci
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-9>>

      <with|par-left|<quote|1tab>|2<space|2spc>Tower of Hanoi
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-10>>
    </associate>
  </collection>
</auxiliary>