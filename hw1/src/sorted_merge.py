#!/usr/bin/env python3

def merge_ab(a, b):
    c = []
    ia = 0
    ib = 0
    while ia < len(a) and ib < len(b):
        if a[ia] < b[ib]:
            c.append(a[ia])
            ia += 1
        else:
            c.append(b[ib])
            ib += 1
    while ia < len(a):
        c.append(a[ia])
        ia += 1
    while ib < len(b):
        c.append(b[ib])
        ib += 1
    return c

def main():
    a = [1, 3, 5, 7, 8, 9]
    b = [2, 3, 4, 6, 10, 11]
    print(merge_ab(a, b))

if __name__ == '__main__':
    main()
