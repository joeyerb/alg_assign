#!/usr/bin/env python3

# Brute-force
def closest_pair_2d_bf (points):
    d = dist_2d (points[0], points[1])
    for point_1 in points:
        for point_2 in points:
            if point_1 != point_2:
                d_ = dist_2d (point_1, point_2)
                if d_ < d:
                    d = d_
    return d

# Divide-and-conquer
def closest_pair_2d (points):
    num = len (points)
    mid = int (num / 2)

    if num == 2:
        return dist_2d (points[0], points[1])

    if num == 3:
        d1 = dist_2d (points[0], points[1])
        d2 = dist_2d (points[1], points[2])
        d3 = dist_2d (points[2], points[0])
        return min (d1, d2, d3)

    # Left and Right
    d1 = closest_pair_2d (points[:mid])
    d2 = closest_pair_2d (points[mid:])
    d = min (d1, d2)

    # Middle
    mid_points = []
    i = mid
    if i >= 0 and points[mid][0] - points[i][0] <= d:   # d[0]
        mid_points.append(points[i])
        i -= 1
    i = mid
    if points[i][0] - points[mid][0] < d:   # d[0]
        mid_points.append(points[i])
        i += 1
    mid_points = sorted (mid_points, key=lambda p: p[1])
    n = len(mid_points)
    for i in range(0, n):
        j = i - 6
        while j < 0:
            j += 1
        while j < i + 7 and i + 7 < n:
            d_ = dist_2d (mid_points[i], mid_points[j])
            if d_ < d:
                d = d_
            j += 1

    return d

def dist_2d (p1, p2):
    return (p1[0] - p2[0]) ** 2 + (p1[1] - p2[1]) ** 2

# Or return a tuple (dist, p1, p2)
def dist_2d_ (p1, p2):
    return ((p1[0] - p2[0]) ** 2 + (p1[1] - p2[1]) ** 2, p1, p2)

def main():
    points = [(1, 2), (17, 11), (11, 0), (26, 33), (38, 9)]
    print (closest_pair_2d (sorted (points)))
    print (closest_pair_2d_bf (points))

if __name__ == '__main__':
    main()
