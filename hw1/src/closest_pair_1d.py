#!/usr/bin/env python3

def closest_pair_1d (points):
    num = len (points)
    mid = int (num / 2)

    if num == 2:
        return dist_1d (points[0], points[1])

    if num == 3:
        d1 = dist_1d (points[0], points[1])
        d2 = dist_1d (points[1], points[2])
        return min (d1, d2)

    d1 = closest_pair_1d (points[:mid])
    d2 = closest_pair_1d (points[mid:])

    d3 = dist_1d (points[mid-1], points[mid])

    return min (d1, d2, d3)

def dist_1d (p1, p2):
    if p1 > p2:
        return p1 - p2
    else:
        return p2 - p1

def main():
    points = [152, 111, 8, 23, 84, 34, 66, 132]
    print (closest_pair_1d (sorted (points)))

if __name__ == '__main__':
    main()
