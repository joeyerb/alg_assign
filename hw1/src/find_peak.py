#!/usr/bin/env python

def find_peak(arr, first, last):
    mid = (first + last) / 2
    if arr[mid-1] > arr[mid]:
        return find_peak(arr, first, mid-1)
    elif arr[mid] < arr[mid+1]:
        return find_peak(arr, mid+1, last)
    else:
        return (mid, arr[mid])

def find_max_in_col(mtrx, col):
    _max = -1
    m_row = 0
    for i in range(1, len(mtrx)-1):
        if mtrx[i][col] > _max:
            _max = mtrx[i][col]
            m_row = i
    return (m_row, _max)

def find_peak2(mtrx, s_row, e_row, s_col, e_col):
    mid_row = (s_row + e_row) / 2
    mid_col = (s_col + e_col) / 2
    row_max = max(mtrx[mid_row])
    m_col = mtrx[mid_row].index(row_max)
    (m_row, col_max) = find_max_in_col(mtrx, mid_col)
    if row_max >= col_max:
        if mtrx[m_col][mid_row-1] > mtrx[m_col][mid_row]:
            if m_col < mid_col:   # top left
                return find_peak2(mtrx, s_row, mid_row, s_col, mid_col)
            else:                   # top right
                return find_peak2(mtrx, s_row, mid_row, mid_col, e_col)
        elif mtrx[m_col][mid_row+1] > mtrx[m_col][mid_row]:
            if m_col < mid_col:   # bottom left
                return find_peak2(mtrx, mid_row, e_row, s_col, mid_col)
            else:                   # bottom right
                return find_peak2(mtrx, mid_row, s_row, mid_col, e_col)
        else:
            return ((m_col, mid_row), mtrx[m_col][mid_row])
    else:
        if mtrx[mid_col-1][m_row] > mtrx[mid_col][m_row]:
            if m_row < mid_row:   # top left
                return find_peak2(mtrx, s_row, mid_row, s_col, mid_col)
            else:                   # bottom left
                return find_peak2(mtrx, s_row, mid_row, mid_col, e_col)
        elif mtrx[mid_col+1][m_row] > mtrx[mid_col][m_row]:
            if m_row < mid_row:   # top right
                return find_peak2(mtrx, mid_row, e_row, s_col, mid_col)
            else:                   # bottom right
                return find_peak2(mtrx, mid_row, s_row, mid_col, e_col)
        else:
            return ((mid_col, m_row), mtrx[mid_col][m_row])
    

def main():
    arr = [10, 3, 3, 2, 8, 7, 5, 6, 5, 4, 3]
    new_arr = [-1] + arr + [-1]
    print(new_arr)
    print(find_peak(new_arr, 1, len(arr)))

    print
    mtrx = [
        [ 4,  5,  6,  7,  8,  7,  6,  5,  4,  3,  2],
        [ 5,  6,  7,  8,  9,  8,  7,  6,  5,  4,  3],
        [ 6,  7,  8,  9, 10,  9,  8,  7,  6,  5,  4],
        [ 7,  8,  9, 10, 11, 10,  9,  8,  7,  6,  5],
        [ 8,  9, 10, 11, 12, 11, 10,  9,  8,  7,  6],
        [ 7,  8,  9, 10, 11, 10,  9,  8,  7,  6,  5],
        [ 6,  7,  8,  9, 10,  9,  8,  7,  6,  5,  4],
        [ 5,  6,  7,  8,  9,  8,  7,  6,  5,  4,  3],
        [ 4,  5,  6,  7,  8,  7,  6,  5,  4,  3,  2],
        [ 3,  4,  5,  6,  7,  6,  5,  4,  3,  2,  1],
        [ 2,  3,  4,  5,  6,  5,  4,  3,  2,  1,  0]
    ]
    mtrx = [
        [ 0,  0,  0,  1,  0,  0, 0],
        [ 0,  4,  3,  2,  0,  1, 0],
        [ 0,  5,  0,  1,  1,  2, 0],
        [ 0,  6,  1,  0,  0,  3, 0],
        [ 0,  7,  0,  0,  0, 13, 0],
        [ 0,  8,  9, 10, 11, 12, 0],
        [ 0,  0,  0,  0,  0,  0, 0]
    ]
    row0 = []
    n_row = len(mtrx)
    n_col = len(mtrx[0])
    for col in range(0, n_col):
        row0 += [-1]
    new_mtrx = [row0]
    for row in mtrx:
        new_mtrx += [[-1] + row + [-1]]
    new_mtrx += [row0]
    for row in new_mtrx:
        print(row)
    print(find_peak2(new_mtrx, 1, n_row, 1, n_col))

if __name__ == '__main__':
    main()
