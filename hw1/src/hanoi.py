#!/usr/bin/env python3

class Hanoi:
    ''' Hanoi Class

        move A[1:n] to C[] :
            1. move A[1:n-1] to B[]
            2. move A[n] to C[]
            3. move B[1:n-1] to C[n]
    '''

    def __init__(self, n):
        self.A = ['A']
        self.B = ['B']
        self.C = ['C']
        self.step = 0
        for i in range(0, n):
            self.A.append(n-i)
        self.hanoi_rec(n, self.A, self.C, self.B)
    
    def hanoi_rec(self, n, src, dest, etc):
        if n == 0:
            return

        # 1. move SRC[:n-1] to ETC[]
        self.hanoi_rec(n-1, src, etc, dest)

        # 2. move SRC[n] to DEST[]
        dest.append(src.pop())

        self.step += 1
        print(n, ': ', src[0], '->', dest[0])
        #print(self.step, ':', n, '->', self.A, self.B, self.C)

        # 3. move ETC[:n-1] to DEST[n]
        self.hanoi_rec(n-1, etc, dest, src)
