#!/usr/bin/env python3

def greedy_wrap(text, width):
    words = text.split()
    count = len(words)
    length = [-1] * count

    j = 0
    k = 0
    line = {}
    while j < count:
        for i in range(j, count):
            if length[k] < width:
                if k not in line.keys():
                    line[k] = []
                #print(k, line[k], '+ ', words[i])
                line[k].append(words[i])
                length[k] += len(words[i]) + 1
                j = i + 1
            if length[k] == width:
                k += 1
                break
            if length[k] > width:
                #print(k, line[k], '- ', words[i])
                line[k].pop()
                length[k] -= len(words[i]) + 1
                j = i
                k += 1
                break
    for l in line:
        print(' '.join(line[l]))

#greedy_wrap('aaa bb cc ddddd', 6)


def dp_wrap(text, width):

    words = text.split()
    count = len(words)

    j = 0
    line = {}
    length = {}
    line[0] = 0
    (line[1], length[0]) = next_line(words, width, line, 0)
    k = 1
    while True:
        tmp_sum = 0
        current_sum = float('Inf')
        for c in range(line[k]-line[k-1]+1):
            _length = length.copy()
            _line = line.copy()
            tmp_sum = try_sum(words, width, _length, _line, k, c)
            if tmp_sum == -1:
                break
            if tmp_sum < current_sum:
                current_sum = tmp_sum
            else:
                line[k] -= c - 1
                (line[k+1], length[k]) = next_line(words, width, line, k)
                k += 1
                break
        if tmp_sum == -1:
            break
    for k in line.keys():
        if k+1 in line.keys():
            to = line[k+1]
        else:
            to = count
        for i in range(line[k], to-1):
            print(words[i], end=' ')
        print(words[to-1])


def try_sum(words, width, length, line, k, c):
    for i in range(c):
        line[k] -= 1
        length[k-1] -= len(words[line[k]]) + 1
    (line[k+1], length[k]) = next_line(words, width, line, k)
    if line[k+1] == -1:
        return -1
    return sum2line(width, length, k)

def next_line(words, width, line, k):
    length = -1
    for i in range(line[k], len(words)):
        length += len(words[i]) + 1
        if length > width:
            return (i, length - len(words[i]) - 1)
    return (-1, length)

def sum2line(width, length, k):
    return (width - length[k-1])**2 + (width - length[k])**2

#dp_wrap('aaa bb cc ddddd ee fff g hhhh iii jjj kk', 8)


t = 'In 1933, according to Historian Arthur Schlesinger, Jr, there was a wide gap of wealth inequality between the Industrial segments of America and the Agricultural regions. During this time banks were foreclosing on farmers left and right. Le Mars caught the attention of the nation when "over 500 hundred farmers crowded the court room in Le Mars" to demand that judge Charles C. Bradley suspend foreclosure proceedings until recently passed laws could be considered. The judge refused. According to Schlesinger a farmer remarked that the court room wasn\'t his alone, that we farmers had paid for it with our taxes. The crowd rushed the judge, slapped him, and drove to a pole and placed a rope around his neck and a hub cap on his head. They did not kill him, however, the people of Le Mars had rated that respecting law unfairly applied was not to be tolerated.'

greedy_wrap(t, 80)

dp_wrap(t, 80)
