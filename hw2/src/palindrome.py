#!/usr/bin/env python3

def is_palindrome(string):
    if len(string) < 2:
        return True
    elif string[0] == string[-1]:
        return is_palindrome(string[1:-1])
    else:
        return False

print(is_palindrome('abcdcba'))
print(is_palindrome('abcddcba'))
print(is_palindrome('1234254321'))
