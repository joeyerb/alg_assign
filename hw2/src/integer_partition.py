#!/usr/bin/env python3

'''
f(6, 6) =
f(6, 5) + 1 =
f(6, 4) + 1 + 1 =
f(6, 3) + f(2, 2) + 1 + 1 =
f(6, 2) + f(3, 3) + f(2, 2) + 1 + 1 =
f(6, 1) + f(4, 2) + f(3, 3) + f(2, 2) + 1 + 1 =
f(6, 1) + f(4, 2) + f(3, 3) + f(2, 4) + f(1, 5) + f(0, 6) =
f(6, 1) + f(4, 2) + f(3, 3) + f(2, 2) + f(1, 1) + f(0, 0)

f(n, m) = f(n, m-1) + f(n-m, min(m, n-m))
'''

def ip(n, m):
    assert m >= 0
    if n < m:
        return ip(n, n)
    if n == 0 or m == 1:
        return 1
    return ip(n, m-1) + ip(n-m, m)

for i in range(0, 11):
    print (ip(i, i), end=', ')
