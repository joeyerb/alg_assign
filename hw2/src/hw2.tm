<TeXmacs|1.99.2>

<style|generic>

<\body>
  <doc-data|<doc-title|Algorithm Analysis and Design>>

  <section*|Week 4>

  <subsection|Hanoi Stack>

  <subsection|Palindromes>

  <\itemize>
    <item>Code

    <\cpp-code>
      #!/usr/bin/env python3

      \;

      def is_palindrome(string):

      \ \ \ \ if len(string) \<less\> 2:

      \ \ \ \ \ \ \ \ return True

      \ \ \ \ elif string[0] == string[-1]:

      \ \ \ \ \ \ \ \ return is_palindrome(string[1:-1])

      \ \ \ \ else:

      \ \ \ \ \ \ \ \ return False

      \;

      print(is_palindrome('abcdcba'))

      print(is_palindrome('abcddcba'))

      print(is_palindrome('1234254321'))

      \;
    </cpp-code>

    <item>Output

    <\cpp-code>
      True

      True

      False
    </cpp-code>

    \;
  </itemize>

  <subsection|Permutation>

  <cpp|ABC =\<gtr\> {ABC, ACB, BCA, BAC, CAB, CBA}>

  <\itemize>
    <item>Code

    <\cpp-code>
      #!/usr/bin/env python3

      \;

      # lsls: list of new lists, d: depth

      def per(lst, head=[], lsls=[], d=0):

      \ \ \ \ if lst:

      \ \ \ \ \ \ \ \ for item in lst:

      \ \ \ \ \ \ \ \ \ \ \ \ rst = list(lst) \ \ \ \ # rst: copy of lst

      \ \ \ \ \ \ \ \ \ \ \ \ rst.remove(item)

      \ \ \ \ \ \ \ \ \ \ \ \ per(rst, head + [item], lsls, d+1)

      \ \ \ \ \ \ \ \ if d == 0:

      \ \ \ \ \ \ \ \ \ \ \ \ return lsls

      \ \ \ \ else:

      \ \ \ \ \ \ \ \ lsls.append(head)

      \;

      print(per([1, 2, 3]))

      \;
    </cpp-code>

    <item>Output

    <\cpp-code>
      [[1, 2, 3], [1, 3, 2], [2, 1, 3], [2, 3, 1], [3, 1, 2], [3, 2, 1]]
    </cpp-code>

    \;
  </itemize>

  <subsection|BST Search>

  <\itemize>
    <item>Pseudocode<\footnote>
      https://en.wikipedia.org/wiki/Binary_search_tree#Searching
    </footnote>
  </itemize>

  <\verbatim-code>
    function Find-recursive(key, node): \ // call initially with node = root

    \ \ \ \ if node = Null or node.key = key then

    \ \ \ \ \ \ \ \ return node

    \ \ \ \ else if key \<less\> node.key then

    \ \ \ \ \ \ \ \ return Find-recursive(key, node.left)

    \ \ \ \ else

    \ \ \ \ \ \ \ \ return Find-recursive(key, node.right)
  </verbatim-code>

  <section*|Week 5>

  <subsection|Master Method>

  <\enumerate-Alpha>
    <item><math|T<around*|(|n|)>=2*T<around*|(|<frac|n|2>|)>+n>

    <\eqnarray*>
      <tformat|<table|<row|<cell|>|<cell|>|<cell|f<around*|(|n|)>=n<eq-number>>>|<row|<cell|a=2\<nocomma\>,b=2>|<cell|\<Rightarrow\>>|<cell|O<around*|(|n<rsup|log<rsub|b>a>|)>=O<around*|(|n<rsup|lg2>|)>=n<eq-number>>>|<row|<cell|<around*|(|1|)>\<nocomma\>,<around*|(|2|)>>|<cell|\<Rightarrow\>>|<cell|f<around*|(|n|)>=O<around*|(|n<rsup|log<rsub|b>a>|)><eq-number>>>|<row|<cell|<around*|(|3|)>>|<cell|\<Rightarrow\>>|<cell|T<around*|(|n|)>=\<theta\><around*|(|n*lg*n|)><eq-number>>>>>
    </eqnarray*>

    <item><math|T<around*|(|n|)>=2*T<around*|(|<frac|n|2>|)>+n<rsup|2>>

    <\eqnarray*>
      <tformat|<table|<row|<cell|>|<cell|>|<cell|f<around*|(|n|)>=n<rsup|2><eq-number>>>|<row|<cell|a=2\<nocomma\>,b=2>|<cell|\<Rightarrow\>>|<cell|O<around*|(|n<rsup|log<rsub|b>a>|)>=O<around*|(|n<rsup|lg2>|)>=n<eq-number>>>|<row|<cell|<around*|(|5|)>\<nocomma\>,<around*|(|6|)>>|<cell|\<Rightarrow\>>|<cell|f<around*|(|n|)>=O<around*|(|n<rsup|log<rsub|b>a+1>|)><eq-number>>>|<row|<cell|>|<cell|>|<cell|a*f<around*|(|<frac|n|b>|)>=2*<around*|(|<frac|n|2>|)><rsup|2>\<leqslant\>C*n<rsup|2>=C*f<around*|(|n|)><eq-number>>>|<row|<cell|<around*|(|7|)>,<around*|(|8|)>>|<cell|\<Rightarrow\>>|<cell|T<around*|(|n|)>=\<theta\><around*|(|n<rsup|2>|)><eq-number>>>>>
    </eqnarray*>

    <item><math|T<around*|(|n|)>=2*T<around*|(|<frac|n|2>|)>+n<rsup|3>>

    <\eqnarray*>
      <tformat|<table|<row|<cell|>|<cell|>|<cell|f<around*|(|n|)>=n<rsup|3><eq-number>>>|<row|<cell|a=2\<nocomma\>,b=2>|<cell|\<Rightarrow\>>|<cell|O<around*|(|n<rsup|log<rsub|b>a>|)>=O<around*|(|n<rsup|lg2>|)>=n<eq-number>>>|<row|<cell|<around*|(|10|)>\<nocomma\>,<around*|(|11|)>>|<cell|\<Rightarrow\>>|<cell|f<around*|(|n|)>=O<around*|(|n<rsup|log<rsub|b>a+2>|)><eq-number>>>|<row|<cell|>|<cell|>|<cell|a*f<around*|(|<frac|n|b>|)>=2*<around*|(|<frac|n|2>|)><rsup|3>\<leqslant\>C*n<rsup|3>=C*f<around*|(|n|)><eq-number>>>|<row|<cell|<around*|(|12|)>,<around*|(|13|)>>|<cell|\<Rightarrow\>>|<cell|T<around*|(|n|)>=\<theta\><around*|(|n<rsup|3>|)><eq-number>>>>>
    </eqnarray*>
  </enumerate-Alpha>

  <subsection|Integer Partition>

  <\itemize>
    <item>Example: f(6, 6)
  </itemize>

  <\mmx-code>
    f(6, 6) =

    f(6, 5) + 1 =

    f(6, 4) + 1 + 1 =

    f(6, 3) + f(2, 2) + 1 + 1 =

    f(6, 2) + f(3, 3) + f(2, 2) + 1 + 1 =

    f(6, 1) + f(4, 2) + f(3, 3) + f(2, 2) + 1 + 1 =

    f(6, 1) + f(4, 2) + f(3, 3) + f(2, 4) + f(1, 5) + f(0, 6) =

    f(6, 1) + f(4, 2) + f(3, 3) + f(2, 2) + f(1, 1) + f(0, 0) =

    \;

    =\<gtr\> f(n, m) = f(n, m-1) + f(n-m, min(m, n-m))

    \;
  </mmx-code>

  <\itemize>
    <item>Code:
  </itemize>

  <\cpp-code>
    #!/usr/bin/env python3

    \;

    def ip(n, m):

    \ \ \ \ assert m \<gtr\>= 0

    \ \ \ \ if n \<less\> m:

    \ \ \ \ \ \ \ \ return ip(n, n)

    \ \ \ \ if n == 0 or m == 1:

    \ \ \ \ \ \ \ \ return 1

    \ \ \ \ return ip(n, m-1) + ip(n-m, m)

    \;

    for i in range(1, 11):

    \ \ \ \ print (ip(i, i), end=', ')

    \;
  </cpp-code>

  <\itemize>
    <item>Output:
  </itemize>

  <\scm-code>
    1, 2, 3, 5, 7, 11, 15, 22, 30, 42, \<#23CE\>
  </scm-code>

  \;
</body>

<\references>
  <\collection>
    <associate|CITEREFGilbergForouzan2001|<tuple|<with|mode|<quote|math>|\<bullet\>>|?>>
    <associate|auto-1|<tuple|?|1>>
    <associate|auto-10|<tuple|2|7>>
    <associate|auto-11|<tuple|2|7>>
    <associate|auto-12|<tuple|2|?>>
    <associate|auto-13|<tuple|2|?>>
    <associate|auto-14|<tuple|1.5|?>>
    <associate|auto-15|<tuple|1.6|?>>
    <associate|auto-16|<tuple|2|?>>
    <associate|auto-2|<tuple|1|1>>
    <associate|auto-3|<tuple|2|3>>
    <associate|auto-4|<tuple|3|3>>
    <associate|auto-5|<tuple|4|4>>
    <associate|auto-6|<tuple|1|4>>
    <associate|auto-7|<tuple|1|5>>
    <associate|auto-8|<tuple|2|6>>
    <associate|auto-9|<tuple|2|6>>
    <associate|footnote-1|<tuple|1|?>>
    <associate|footnr-1|<tuple|1|?>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|toc>
      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|Week
      4> <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-1><vspace|0.5fn>

      <with|par-left|<quote|1tab>|1<space|2spc>Hanoi Stack
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-2>>

      <with|par-left|<quote|1tab>|2<space|2spc>Palindromes
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-3>>

      <with|par-left|<quote|1tab>|3<space|2spc>Permutation
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-4>>

      <with|par-left|<quote|1tab>|4<space|2spc>BST Search
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-5>>

      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|Week
      5> <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-6><vspace|0.5fn>

      <with|par-left|<quote|1tab>|1<space|2spc>Master Method
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-7>>

      <with|par-left|<quote|1tab>|2<space|2spc>Integer Partition
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-8>>
    </associate>
  </collection>
</auxiliary>