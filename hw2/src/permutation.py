#!/usr/bin/env python3

def per (lst, head=[], lsls=[], d=0):
    if lst:
        for item in lst:
            rst = list(lst)
            rst.remove(item)
            per(rst, head + [item], lsls, d+1)
        if d == 0:
            return lsls
    else:
        lsls.append(head)


print(per([1, 2, 3]))
