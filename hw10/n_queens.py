#!/usr/bin/env python3

def solve_puzzle(n):
    result = [-1] * n
    y = 0
    while True:
        for k in range(y+1, n):
            result[k] = -1
        if y == 0 and result[0] == n-1:
            break
        result[y] = find_x(y, n, result)
        if result[y] < 0:
            y -= 1
        elif y+1 < n:
            y += 1
        else:
            yield result

def find_x(y, n, result):
    for x in range(result[y]+1, n):
        if is_legal(x, y, result):
            return x
    return -1

def is_legal(x, y, result):
    for j in range(y):
        if result[j] in (j+x-y, x, x+y-j):
            return False
    return True

for solution in solve_puzzle(4):
    print(solution)

# each solution is a list:
#
#                   + O + +
# [1, 3, 0, 2]  =>  + + + O
#                   O + + +
#                   + + O +
