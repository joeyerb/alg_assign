#!/usr/bin/env python3

H = 'Heart'
D = 'Diamond'
C = 'Club'
S = 'Spade'
J = 11
Q = 12
K = 13
A = 1

tuple_add = lambda xs,ys: tuple(x + y for x, y in zip(xs, ys))

# Input
cards = [(7, H), (6, H), (7, D), (3, D), (8, C), (J, S)]

n = len(cards)

memo = {}   # {i: (length, next_card), ...}

def max_match(i):
    _max = (0, n)
    for j in range(i+1, n):
        if cards[i][1] == cards[j][1] or \
                cards[i][0] == cards[j][0] or \
                cards[i][0] == 8 or \
                cards[j][0] == 8:
                    if crazy(j)[0] > _max[0]:
                        _max = (crazy(j)[0], j)
    return _max

def crazy(i):
    if i in memo:
        return memo[i]
    else:
        memo[i] = tuple_add((1, 0), max_match(i))
        return memo[i]

crazy(0)

print(memo)

# Output
c = max(memo, key=lambda x:memo[1])
while c < n:
    print(cards[c])
    c = memo[c][1]

