#!/usr/bin/env python3

def fib(n):
    a = 0
    b = 1
    for i in range(n):
        c = a + b
        a = b
        b = c
    return a

for i in range(9):
    print(i, fib(i))

print(fib(100))
